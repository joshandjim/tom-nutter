<?php
namespace Deployer;

require 'recipe/common.php';

// Start editable

set('repository', 'git@bitbucket.org:joshandjim/tom-nutter.git');
set('theme_dir', 'tomnutter-theme');

set('writable_use_sudo', false);
set('writable_mode', 'chmod');

// End editable

// Do not modify anything under this line unless you know what you're doing

inventory('hosts.yml');
set('local_theme', __DIR__ . '/web/app/themes/{{theme_dir}}');
set('shared_dirs', [
  'web/app/uploads',
  'web/app/plugins/bwp-minify/cache',
]);
set('shared_files', ['.env', 'web/.htaccess']);

task('deploy:gulp', function() {
  $do_gulp = askConfirmation('Run Gulp?', false);
  if( $do_gulp ) { runLocally('cd {{local_theme}} && gulp --production'); }
})->desc('Create dist folder');

task('deploy:temp_symlink', function() {
  run('ln -s {{deploy_path}}/shared/.env {{release_path}}/.env');
});

task('deploy:upload_dist', function() {
  upload('{{local_theme}}/dist', '{{release_path}}/web/app/themes/{{theme_dir}}/');
})->desc('Upload dist folder to server');

task('deploy:theme_composer', function() {
  cd('{{release_path}}/web/app/themes/{{theme_dir}}');
  run('composer install');
})->desc('Remote composer install');

task('deploy:remove_themes', function () {
  cd('{{release_path}}');
  run('rm -rf ./web/wp/wp-content/themes/*');
});

task('setup', [
  'deploy:prepare',
])->desc('Inital deployment setup');

task('cloudflare', function() {
  if (get('branch') == 'master') :
    run('curl https://www.cloudflare.com/api_json.html \
  -d "a=zone_file_purge" \
   -d "tkn={{api_key}}" \
   -d "email=yadda@yadda.co.uk" \
   -d "z=%URL_WITHOUT_HTTP%" \
   -d "url=%URL_WITH_HTTP%/app/themes/{{theme_dir}}/dist/css/main.css"');
   run('curl https://www.cloudflare.com/api_json.html \
   -d "a=zone_file_purge" \
   -d "tkn={{api_key}}" \
   -d "email=yadda@yadda.co.uk" \
   -d "z=%URL_WITHOUT_HTTP%" \
   -d "url=%URL_WITH_HTTP%/app/themes/{{theme_dir}}/dist/js/main.js"');
 else:
   write('Not production, skipping cloudflare');
 endif;
})->desc('Empty cache of live site');

task('deploy', [
  'deploy:gulp',
  'deploy:release',
  'deploy:update_code',
  'deploy:temp_symlink',
  'deploy:vendors',
  'deploy:upload_dist',
  'deploy:theme_composer',
  'deploy:shared',
  'deploy:symlink',
  'deploy:remove_themes',
  'cleanup'
])->desc('Executing Deploy task');

after('deploy', 'cloudflare');
