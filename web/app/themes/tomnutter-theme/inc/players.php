<?php
// Players Post Type
add_action('init', function () {
  $result = register_post_type('player', [
    'label' => 'Players',
    'public' => true,
    'query_var' => true,
    'has_archive' => true,
    'publicly_queryable' => true,
    'hierarchical' => true,
    'menu_icon' => 'dashicons-groups',
    'supports' => array(
      'title'
    )
  ]);
});
