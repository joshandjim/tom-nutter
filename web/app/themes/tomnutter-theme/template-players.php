<?php

/**
 * Template Name: Players
 */

 global $post;

 get_header();

 include 'templates/content-page-players.php';

 get_footer();
