<?php

/**
 * Template Name: Services Subpage
 */

 global $post;

 get_header();

 include 'templates/content-page-services-subpage.php';

 get_footer();
