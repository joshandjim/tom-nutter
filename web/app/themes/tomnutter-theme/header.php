<?php
use \Tofino\ThemeOptions\Menu as m;
use \Tofino\ThemeOptions\Notifications as n; ?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php wp_head(); ?>
  <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700|Oswald:400,600,700" rel="stylesheet">
</head>
<body <?php body_class(); ?>>

<?php n\notification('top'); ?>

<!--[if lte IE 9]>
  <div class="alert alert-danger browser-warning">
    <p><?php _e('You are using an <strong>outdated</strong> browser. <a href="http://browsehappy.com/">Update your browser</a> to improve your experience.', 'tofino'); ?></p>
  </div>
<![endif]-->

<nav class="navbar navbar-light navbar-toggleable-lg <?php echo m\menu_sticky(); ?> <?php echo m\menu_position(); ?>">

  <div class="social-header-wrapper hidden-xs-down">
    <?php echo social_icons(['class' => "social-icons"]); ?>
  </div>

  <div class="header-logo">
    <a class="navbar-brand" href="<?php echo home_url(); ?>" title="<?php echo esc_attr(bloginfo('name')); ?>">
      <?php echo svg([
        'sprite' => 'main-icon',
        'class' => 'main-icon',
      ]); ?>
      <span class="sr-only">
        <?php echo bloginfo('name'); ?>
      </span>
    </a>
  </div>

  <div class="collapse navbar-collapse navbar-wrapper header-nav  navbar-right" id="main-menu">
    <?php
    if (has_nav_menu('primary_navigation')) :
      wp_nav_menu([
        'menu'            => 'nav_menu',
        'theme_location'  => 'primary_navigation',
        'depth'           => 2,
        'container'       => '',
        'container_class' => '',
        'container_id'    => '',
        'menu_class'      => 'navbar-nav',
        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        'walker'          => new Tofino\Nav\NavWalker()
      ]);
    endif; ?>
  </div>

  <button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse" data-target="#main-menu">
    <span class="bar-wrapper">
      <span class="bar"></span>
      <span class="bar"></span>
      <span class="bar"></span>
    </span>
    <span class="sr-only"><?php _e('Toggle Navigation Button', 'tofino'); ?></span>
  </button>
</nav>

<?php if (get_theme_mod('footer_sticky') === 'enabled') : ?>
  <div class="wrapper">
<?php endif; ?>
