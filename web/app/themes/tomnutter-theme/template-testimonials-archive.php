<?php

/**
 * Template Name: Testimonials Archive
 */

 global $post;

 get_header();

 include 'templates/content-page-testimonials.php';

 get_footer();
