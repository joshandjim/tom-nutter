<?php

/**
 * Template Name: About
 */

 global $post;

 get_header();

 include 'templates/content-page-about.php';

 get_footer();
