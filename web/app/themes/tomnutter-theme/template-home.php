<?php

/**
 * Template Name: Home
 */

 global $post;

 get_header();

 include 'templates/content-page-home.php';

 get_footer();
