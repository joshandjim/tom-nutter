<?php
use \Tofino\ThemeOptions\Notifications as n;

if (get_theme_mod('footer_sticky') === 'enabled') : ?>
  </div>
<?php endif; ?>

<footer class="pitch-line-border-top bg-black">
  <div class="social-wrapper">
    <?php echo social_icons(['class' => "social-icons social-icons--bar"]); ?>
  </div>

  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-4 offset-lg-2 footer-left-col">
          <a class="navbar-brand navbar-brand-footer footer_pitch-line" href="<?php echo home_url(); ?>" title="<?php echo esc_attr(bloginfo('name')); ?>">
            <?php echo svg([
              'sprite' => 'main-logo',
              'class' => 'main-logo',
            ]); ?>
            <span class="sr-only">
              <?php echo bloginfo('name'); ?>
            </span>
          </a>

          <a class="footer-contact alt-text alt-text-lower" href="mailto:<?php echo ot_shortcode(['id' => 'email_address']); ?>">
            <?php echo svg([
              'sprite' => 'email',
              'class' => 'email-icon',
            ]); ?>

            <?php echo ot_shortcode('email_address'); ?>
          </a>

          <a class="footer-contact alt-text tel-link rulertel" href="tel:<?php echo ot_shortcode('telephone_number'); ?>">
            <?php echo svg([
              'sprite' => 'phone',
              'class' => 'phone-icon',
            ]); ?>
            <?php echo ot_shortcode('telephone_number'); ?>
          </a>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4 footer-nav">
          <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu([
              'menu'            => 'nav_menu',
              'theme_location'  => 'primary_navigation',
              'depth'           => 2,
              'container'       => '',
              'container_class' => '',
              'container_id'    => '',
              'menu_class'      => 'navbar-nav',
              'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
              'walker'          => new Tofino\Nav\NavWalker()
            ]);
          endif; ?>
        </div>
      </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>

<?php n\notification('bottom'); ?>

</body>
</html>
