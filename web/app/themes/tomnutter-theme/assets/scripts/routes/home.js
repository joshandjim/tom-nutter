// We need jQuery
var $ = window.jQuery;

// Import Instafeed
var Instafeed = require("instafeed.js");

export default {
  init() {
    //Carousel
    $('.carousel-image').slick({
     slidesToShow: 1,
     slidesToScroll: 1,
     arrows: false,
     centerMode: true,
     fade: false,
     asNavFor: '.carousel'
    });

    $(".carousel").slick({
      autoplay: false,
      autoplaySpeed: 3000,
      fade: true,
      speed: 300,
      slidesToShow: 1,
      centerMode: true,
      prevArrow: $('.prev'),
      nextArrow: $('.next'),
      arrows: true,
      dots: false,
      asNavFor: '.carousel-image',
    });

    var userFeed = new Instafeed({
        get: 'user',
        userId: '1559368465',
        clientId: '41f884aa46f54478b57b75110a18a509',
        accessToken: '1559368465.1677ed0.6df30b62864b461ea0f92f038dbb8d83',
        sortBy: 'most-recent',
        limit: '6',
        resolution: 'standard_resolution',
        template: '<div class="col-xs-12 col-md-6 col-lg-4"><a href="{{link}}" class="instagram-image" target="_blank" rel="nofollow"><img src="{{image}}" /></a></div>'
    });

    userFeed.run();
  },
  loaded() {
    // Javascript to be fired on page once fully loaded
  }
};
