<?php

/**
 * Template Name: Contact
 */

 global $post;

 get_header();

 include 'templates/content-page-contact.php';

 get_footer();
