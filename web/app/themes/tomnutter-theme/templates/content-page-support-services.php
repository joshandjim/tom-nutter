<?php $contact_page_id = 18; ?>

<main class="bg-dark">
	<?php get_template_part('templates/partials/page-hero'); ?>
	 	<?php
		$children = get_children([
		  'post_parent' => get_the_ID(),
		  'post_type' => 'page',
		  'orderby' => 'menu_order',
		  'order' => 'ASC',
		]); ?>

		<?php if (count($children)) : ?>
		  <?php foreach ($children as $post) : setup_postdata($post); ?>
		    <?php get_template_part('templates/content-page-services-subpage'); ?>
		  <?php endforeach; ?>
		<?php endif; wp_reset_query(); ?>

	<?php if ($post = get_post($contact_page_id)) :?>
		<?php get_template_part('templates/partials/contact-form'); ?>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
</main>
