<?php if( have_rows('two_column_content') ): ?>
  <?php get_template_part('templates/partials/two-column-content'); ?>
<?php endif; ?>

<?php if (have_rows('service_item')): ?>
  <section class="services-section">
    <div class="container">
      <div class="row">
        <?php while ( have_rows('service_item') ) : the_row(); ?>
        <div class="col-xs-12 col-md-6 services-section_wrapper">
          <div class="services-section_item">
            <h4 class="pitch-line-heading_left"><span><?php the_sub_field('service_title'); ?></span></h4>
            <div class="support-services_icons">
              <?php echo svg([
              'sprite' => 'hex',
              'class' => 'icon-hex'
              ]); ?>
              <?php echo svg([
                'sprite' => 'instagram',
                'class' => 'support-icon',
              ]); ?>
            </div>
            <div class="content-right">
              <?php the_sub_field('service_text'); ?>
            </div>
          </div>
        </div>
        <?php endwhile; ?>
      </div>
    </div>
  </section>  
<?php endif; ?>