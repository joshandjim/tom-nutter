<main class="bg-dark">
  <?php get_template_part('templates/partials/page-hero'); ?>

  <section class="pitch-line-border-top triangle players-section">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-8 offset-md-2">
          <div class="the-content">
            <?php the_content(); ?>
          </div>
        </div>
      </div>

      <?php
      $args = array(
        'post_type'      => 'player',
        'post_status'    => 'publish',
        'orderby'        => 'menu_order',
        'order'          => 'ASC',
        'posts_per_page' => '-1',
        'meta_query'     => [
          [
            'key'     => 'currently_representing',
            'compare' => '==',
            'value'    => 1,
            ],
            ],
          );

          $players = get_posts($args);
          ?>

        <?php if ($players) : ?>
          <div class="row">
            <?php foreach ($players as $post) : setup_postdata($post); ?>
              <div class="col-xs-12 col-md-6">
                <div class="player-profile pitch-line-border">
                  <?php $thumb = get_field('image'); ?>

                  <img class="player-image player-image--profile pitch-line-border" src="<?php echo $thumb['sizes']['player_image'] ?>" alt="<?php echo $thumb['alt'] ?>">

                  <h2 class="player__name">
                    <?php the_field('first_name'); ?>
                    <?php the_title(); ?>
                  </h2>

                  <?php if (get_field('position')) : ?>
                    <h4 class="player__position alt-text alt-text-lower alt-text-sentence ">
                      <?php the_field('position'); ?>
                    </h4>
                  <?php endif; ?>


                  <div class="player-profile__details">
                    <!-- <div class="row"> -->
                      <div class="player-profile__details-col">
                        <?php if (get_field('entry_year')) : ?>
                          <div class="">
                            <span class="alt-text"><span class="hidden-md-down">Entry</span> Year</span>
                            <?php the_field('entry_year'); ?>
                          </div>
                        <?php endif; ?>

                        <?php if (get_field('foot')) : ?>
                          <div class="">
                            <span class="alt-text">Foot</span>
                            <?php the_field('foot'); ?>
                          </div>
                        <?php endif; ?>
                      </div>

                      <div class="player-profile__details-col">
                        <?php if (get_field('gpa_score')) : ?>
                          <div>
                            <span class="alt-text">GPA</span>
                            <?php the_field('gpa_score'); ?>
                          </div>
                        <?php endif; ?>

                        <?php if (get_field('act_score')) : ?>
                          <div>
                            <span class="alt-text">ACT</span>
                            <?php the_field('act_score'); ?>
                          </div>
                        <?php elseif (get_field('sat_score')) : ?>
                          <div>
                            <span class="alt-text">SAT</span>
                            <?php the_field('sat_score'); ?>
                          </div>
                        <?php endif; ?>
                      </div>
                    <!-- </div> -->
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
            <?php wp_reset_postdata(); ?>
          </div>
        <?php endif; ?>
      </div>

  </section>
</main>
