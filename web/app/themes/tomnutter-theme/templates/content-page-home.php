<?php $contact_page_id = 18; ?>

<main class="bg-dark">
  <?php get_template_part('templates/partials/homepage-hero'); ?>

  <?php if (have_rows('two_column_content')): ?>
    <?php get_template_part('templates/partials/two-column-content'); ?>
  <?php endif; ?>

  <?php get_template_part('templates/partials/testimonials-carousel'); ?>

  <?php if ($post = get_post($contact_page_id)) : setup_postdata($post); ?>
    <?php get_template_part('templates/partials/contact-form'); ?>
  <?php endif; ?>
  <?php wp_reset_postdata(); ?>

  <?php get_template_part('templates/partials/instagram'); ?>

</main>
