<?php $thumb = get_field('hero_image'); ?>
<div class="hero hero-home" style="background-image: url(<?php echo $thumb['sizes']['large'] ?>)">
	<div class="hero-content">
		<h1>
			<?php echo svg([
				'sprite' => 'main-logo',
				'class' => 'main-logo',
			]); ?>

			<span class="sr-only">
        <?php echo bloginfo('name'); ?>
      </span>
		</h1>
		<div class="hero-content_title hero-content_title--home">
		 	<h2 class="h1 hero-content_pitch-line hero-content_pitch-line--hero alt-text">
				<?php the_field('subtitle'); ?>
			</h2>
		</div>
		<div class="hero-content_cta">
			<a class="btn alt-text" href="<?php the_field('cta_link'); ?>">Find out more</a>
		</div>
	</div>
</div>
