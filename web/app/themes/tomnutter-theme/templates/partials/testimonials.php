<?php
  $args = array(
    'post_type'      => 'testimonial',
    'post_status'    => 'publish',
    'orderby'        => 'menu_order',
    'order'          => 'ASC',
    'posts_per_page' => '-1',
  );

  $testimonials = get_posts($args);
  ?>

<?php if ($testimonials) : ?>
  <div class="row">
    <?php foreach ($testimonials as $post) : setup_postdata($post); ?>
      <div class="col-xs-12 col-md-6">
				<?php $thumb = get_field('image'); ?>
				  <img class="" src="<?php echo $thumb['sizes']['medium'] ?>" alt="<?php echo $thumb['alt'] ?>"
				    srcset="<?php echo $thumb['sizes']['medium'] ?> 1920w, <?php echo $thumb['sizes']['small'] ?> 960w" >
				<?php the_field('quote'); ?>
				<?php the_title(); ?>
				<?php the_field('subtitle'); ?>
			</div>
    <?php endforeach; ?>
    <?php wp_reset_postdata(); ?>
  </div>
<?php endif; ?>