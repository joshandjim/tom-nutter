<?php if (have_rows('two_column_content')) : ?>
  <section class="two-column-content-section pitch-line-border-top triangle">
    <div class="container">
      <?php $row_count = 0; ?>
      <?php while (have_rows('two_column_content')) : the_row(); ?>
        <div class="two-column-content-row">
          <div class="row">
            <div class="col-xs-12 col-md-6 <?php echo ($row_count % 2 ? '' : 'push-md-6')?>">
              <div class="<?php echo ($row_count % 2 == 1 ? 'two-column-content-item two-column-content-item_left' : 'two-column-content-item two-column-content-item_right') ?>">

                <h2 class="pitch-line-heading"><?php the_sub_field('row_heading'); ?></h2>

                <?php the_sub_field('row_text'); ?>

                <?php if ($link = get_sub_field('row_cta_link')) : ?>
                  <div class="content_cta">
                    <a class="btn btn-light alt-text" href="<?php echo $link ?>">
                    <?php the_sub_field('row_cta_text') ?>
                    </a>
                  </div>
                <?php endif ?>
              </div>
            </div>
            <div class="col-xs-12 col-md-6 <?php echo ($row_count % 2 ? '' : 'pull-md-6')?>">
              <div class="<?php echo ($row_count % 2 == 0 ? 'two-column-content-item two-column-content-item_left' : 'two-column-content-item two-column-content-item_right') ?>">

                <?php $thumb = get_sub_field('row_image'); ?>

                <div class="two-column-content_pitch-line">
                  <div class="two-column-content_image" style="background-image: url(<?php echo $thumb['sizes']['large'] ?>)"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php $row_count++ ; ?>
      <?php endwhile; ?>
    </div>
  </section>
<?php endif ?>
