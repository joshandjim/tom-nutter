<div class="carousel-image">
  <?php while (have_rows('featured_testimonials')) : the_row(); ?>
    <div class="slide">
      <?php $post_object = get_sub_field('testimonial'); ?>
      <?php if ($post_object) : ?>
        <?php
        $post = $post_object;
        setup_postdata($post);
        ?>

        <?php if ($thumb = get_field('image')) : ?>
          <img class="player-image pitch-line-border" src="<?php echo $thumb['sizes']['player_image'] ?>" alt="<?php echo $thumb['alt'] ?>">
        <?php endif; ?>
      <?php endif; ?>
    </div>
    <?php wp_reset_postdata(); ?>
  <?php endwhile; ?>
</div>
