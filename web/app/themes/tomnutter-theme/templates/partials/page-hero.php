<?php $thumb = get_field('hero_image'); ?>
<div class="hero hero-page" style="background-image: url(<?php echo $thumb['sizes']['large'] ?>)">
	<div class="hero-content">
		<div class="hero-content_title">
	  	<h1 class="hero-content_pitch-line"><?php the_title(); ?></h1>
	  </div>
	</div>
</div>
