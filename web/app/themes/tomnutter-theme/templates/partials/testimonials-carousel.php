<?php if (have_rows('featured_testimonials')) : ?>
  <?php get_template_part('templates/partials/testimonials-carousel-image'); ?>

  <?php $thumb = get_field('carousel_bg_image'); ?>
  <section class="carousel-section pitch-line-border-top" style="background-image: url(<?php echo $thumb['sizes']['large'] ?>)">
    <div class="shaded-bg">

      <div class="container">
        <div class="quote">
          <?php echo svg([
            'sprite' => 'quote',
            'class' => 'quote-mark',
          ]); ?>
        </div>
      </div>

      <?php get_template_part('templates/partials/testimonials-carousel-content'); ?>

      <ul class="nav-arrows">
        <li class="prev">
          <span class="arrow-icon">
            <?php echo svg([
              'sprite' => 'hex',
              'class' => 'arrow-hex',
            ]); ?>

            <span class="bar"></span>
            <span class="bar bar-bottom"></span>
          </span>
        </li>
        <li class="next">
          <span class="arrow-icon">
            <?php echo svg([
              'sprite' => 'hex',
              'class' => 'arrow-hex',
            ]); ?>

            <span class="bar"></span>
            <span class="bar bar-bottom"></span>
          </span>
        </li>
      </ul>

      <div class="view-all">
        <a href="/testimonials" class="alt-text">View all</a>
      </div>

    </div>
  </section>
<?php endif; ?>
