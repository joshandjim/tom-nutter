<section class="contact-section pitch-line-border-top triangle bg-black">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-8 offset-md-2">
        <h2 class="pitch-line-heading">Get in touch</h2>

        <div class="contact-content">
          <?php the_content(); ?>

          <a class="alt-text tel-link rulertel" href="tel:<?php echo ot_shortcode('telephone_number'); ?>">
            <?php echo svg([
              'sprite' => 'phone',
              'class' => 'phone-icon',
            ]); ?>
            <?php echo ot_shortcode('telephone_number'); ?>
          </a>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="js-form-result"></div>
    <form name="contact" class="contact-form pitch-line-border" data-wp-action="contact-form">
      <div class="row">
      <!-- Empty div for form response -->
        <div class="col-xs-12 col-lg-6">
          <!-- Name / Text input -->
          <fieldset class="form-group required">
            <label class="form-control-label sr-only" for="name"><?php _e('Name', 'tofino'); ?></label>
            <input type="text" name="name" class="form-control" id="name" placeholder="<?php _e('Name', 'tofino'); ?>" required="required">
          </fieldset>

          <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-12">
              <!-- Email address -->
              <fieldset class="form-group required">
                <label class="form-control-label sr-only" for="email"><?php _e('Email', 'tofino'); ?></label>
                <input type="email" name="email" class="form-control" id="email" placeholder="<?php _e('Email', 'tofino'); ?>" required="required">
              </fieldset>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-12">
              <!-- Telephone number -->
              <fieldset class="form-group required">
                <label class="form-control-label sr-only" for="phone"><?php _e('Email', 'tofino'); ?></label>
                <input type="text" name="phone" class="form-control" id="phone" placeholder="<?php _e('Phone', 'tofino'); ?>" required="required">
              </fieldset>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-lg-6">
            <!-- Message / Textarea -->
            <fieldset class="form-group required">
              <label class="form-control-label sr-only" for="message"><?php _e('Message', 'tofino'); ?></label>
              <textarea class="form-control text-area" name="message" id="message" rows="11" placeholder="<?php _e('Message...', 'tofino'); ?>" required="required"></textarea>
            </fieldset>

            <?php if (true == get_theme_mod('contact_form_captcha')) : ?>
              <!-- Not a Robot -->
              <fieldset class="form-group">
                <div class="g-recaptcha" data-size="normal" data-theme="light" data-sitekey="<?php echo get_theme_mod('captcha_site_key'); ?>"></div>
                <small class="text-muted"><?php _e('Human tester', 'tofino'); ?></small>
                <script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>
              </fieldset><?php
            endif; ?>
          </div>
        </div>

        <div class="submit-button">
          <button type="submit" class="btn btn-light alt-text"><?php _e('Send', 'tofino'); ?></button>
        </div>
      </form>
    </div>
</section>
