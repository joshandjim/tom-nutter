<section class="instagram-section pitch-line-border-top triangle triangle-bottom bg-medium">
  <?php echo svg([
    'sprite' => 'instagram',
    'class' => 'instagram-icon',
  ]); ?>

  <h2 class="pitch-line-heading">
    Latest Support
  </h2>
  <div class="container">
    <div id="instafeed" class="row"></div>

    <a href="#" class="instagram-cta btn btn-light alt-text">Follow Us</a>
  </div>
</section>
