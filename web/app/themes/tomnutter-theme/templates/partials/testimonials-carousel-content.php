<div class="container">
  <div class="carousel">
    <?php while (have_rows('featured_testimonials')) : the_row(); ?>
      <?php $post_object = get_sub_field('testimonial'); ?>
      <?php if ($post_object) : ?>
        <div class="slide">
          <?php
          $post = $post_object;
          setup_postdata($post);
          ?>

          <div class="container slide-container">
            <div class="row">
              <div class="col-xs-12 col-md-10 offset-md-1">
                <div class="slide-quote-content">
                  <div class="slide-quote">
                    <?php the_field('quote_excerpt'); ?>
                  </div>

                  <div class="quote-author">
                    <div class="alt-text author-name">
                      <span><?php the_field('first_name'); ?></span>
                      <?php the_title(); ?>
                    </div>

                    <div class="alt-text">
                      <?php the_field('author_info'); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <?php wp_reset_postdata(); ?>
    <?php endwhile; ?>
  </div>
</div>
