<?php $contact_page_id = 18; ?>

<main class="bg-dark">
  <?php get_template_part('templates/partials/page-hero'); ?>

  <?php if (have_rows('two_column_content')): ?>
    <?php get_template_part('templates/partials/two-column-content'); ?>
  <?php endif; ?>

  <?php get_template_part('templates/partials/testimonials-carousel'); ?>

  <?php get_template_part('templates/partials/instagram'); ?>

  <?php if ($post = get_post($contact_page_id)) :?>
    <?php get_template_part('templates/partials/contact-form'); ?>
  <?php endif; ?>
  <?php wp_reset_postdata(); ?>
</main>
