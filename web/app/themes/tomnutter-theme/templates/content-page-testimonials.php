<main class="bg-dark">
  <?php get_template_part('templates/partials/page-hero'); ?>

  <?php
  $args = array(
    'post_type'      => 'player',
    'post_status'    => 'publish',
    'orderby'        => 'menu_order',
    'order'          => 'ASC',
    'posts_per_page' => '-1',
    'meta_query'     => [
       'relation' => 'OR',
       [
          'key'   => 'full_quote',
          'compare' => '!=',
          'value' => NULL,
       ],
       [
          'key'     => 'quote_excerpt',
          'compare' => '!=',
          'value'    => NULL,
          ],
       ],
    );

  $testimonials = get_posts($args);
  ?>

  <div class="container pitch-line-border-top triangle">
    <?php if ($testimonials) : ?>
      <div class="row">
        <?php foreach ($testimonials as $post) : setup_postdata($post); ?>
          <div class="col-xs-12 col-md-6">
            <?php $thumb = get_field('image'); ?>
            <img class="" src="<?php echo $thumb['sizes']['medium'] ?>" alt="<?php echo $thumb['alt'] ?>"
            srcset="<?php echo $thumb['sizes']['medium'] ?> 1920w, <?php echo $thumb['sizes']['small'] ?> 960w" >

            <?php if (get_field('full_quote')) : ?>
              <?php the_field('full_quote'); ?>
            <?php elseif (get_field('quote_excerpt')) : ?>
              <?php the_field('quote_excerpt'); ?>
            <?php endif; ?>

            <?php the_field('first_name'); ?>
            <?php the_title(); ?>
            <?php the_field('author_info'); ?>
          </div>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
      </div>
    <?php endif; ?>
  </div>
</main>
