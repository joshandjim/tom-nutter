<?php

/**
 * Template Name: Support Services
 */

 global $post;

 get_header();

 include 'templates/content-page-support-services.php';

 get_footer();
